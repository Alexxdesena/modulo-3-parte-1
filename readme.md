Critérios de avaliação:

C1. Estrutura correta de seções - 30.

C2. Estrutura correta das listas - 20.

C3. As imagens de decoração é de conteúdo são adicionadas - 10.

C4. As imagens de conteúdo (ícones de redes sociais) têm links - 10.

C5. As imagens de conteúdo (ícones de redes sociais) têm texto alternativo - 10.

C6. Os estilos são habilitados - 20.

- Mentorama 
  